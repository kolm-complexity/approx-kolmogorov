import React from 'react'
import ReactDOM from 'react-dom'
//import reportWebVitals from './reportWebVitals'
import { HashRouter, Routes, Route } from 'react-router-dom'
import 'semantic-ui-less/semantic.less'
import 'katex/dist/katex.min.css';
import './index.css'

import App, { Main } from './App'
import SortedExperiment from './routes/s9'
import UnsortedExperiment from './routes/lookahead'
import ResourcesIMP from './routes/imp'

ReactDOM.render(
    <HashRouter>
        <Routes>
            <Route path='/' element={<App />}>
                <Route index element={<Main />} />
                <Route path='s9' element={<SortedExperiment />} />
                <Route path='lookahead' element={<UnsortedExperiment />} />
                <Route path='imp' element={<ResourcesIMP />} />
                {/* <Route path='enums' element={<ResourcesEnums />} /> */}
                <Route path='*' element={<App />} />
            </Route>
        </Routes>
    </HashRouter>,
    document.getElementById('root')
)

//reportWebVitals()
