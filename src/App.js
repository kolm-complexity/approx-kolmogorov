import React, { Fragment } from 'react'

import {
    Button,
    Segment,
} from 'semantic-ui-react'

import Abstract from './components/Abstract'
import Results from './components/Results'
import Resources from './components/Resources'
import References from './components/References'
import PaperHeading from './components/PaperHeading'
import Footer from './components/Footer'
import { Outlet, useLocation } from 'react-router-dom'
import { HashLink } from 'react-router-hash-link'

const Main = () => {
    return (
        <Fragment>
            <Segment
                inverted
                textAlign='center'
                vertical
                style={{ paddingTop: '3.5rem' }}
            >
                <PaperHeading />
            </Segment>
            <Segment vertical>
                <Abstract />
                <Results />
                <Resources />
            </Segment>
            <Segment vertical>
                <References />
            </Segment>
        </Fragment>
    )
}

const toLocation = (loc) => {
    if (loc === '/') {
        return '/'
    }
    return '/#' + loc.pathname.split('/')[1]
}

const App = () => {
    const loc = useLocation();
    return (
        <Fragment>
            <Button
                circular
                color='grey'
                icon='home'
                size='large'
                as={HashLink}
                to={toLocation(loc)}
                style={{
                    position: 'fixed',
                    top: '.5rem',
                    left: '.5rem',
                    zIndex: '1000',
                }}
            />
            <Outlet />
             <Segment inverted style={{ marginTop: '2rem' }}>
                <Footer />
            </Segment>
        </Fragment>
    )
}

export { Main }
export default App

