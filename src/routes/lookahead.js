import React, { useState } from 'react'
import { InlineMath } from 'react-katex'
import {
    Container,
    Header,
    Message,
    Segment,
    Table,
    Button,
    Icon,
    List,
    Divider,
} from 'semantic-ui-react'

const UnsortedExperiment = () => {
    return (
        <main>
            <Segment
                inverted
                textAlign='center'
                padded
            >
                <Container text>
                    <Header as='h2' inverted>
                        Looking ahead the program space
                    </Header>
                </Container>
            </Segment>
            <FourFamilies />
            <PowersOfTwo />
            <Factorial />
            <Exponentiation1 />
            <Exponentiation2 />
        </main>
    )
}

export default UnsortedExperiment

const PaginatedTable = ({data, n, name, family}) => {
    const total = data.length
    const lastPage = Math.floor(total / n)
    const [page, setPage] = useState(0)
    const slice = data.slice(n*page, n*page+n)
    const [selection, setSelection] = useState(null)

    return (
        <Table
            selectable
            fixed
            compact='very'
            textAlign='center'
            className='force-center'
        >
            <Table.Header className='sans'>
                <Table.Row>
                    <Table.HeaderCell width={2}><InlineMath math='n' /></Table.HeaderCell>
                    <Table.HeaderCell width={3}>Time</Table.HeaderCell>
                    <Table.HeaderCell width={11}>Output</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {slice.map((d) => (
                    <Table.Row
                        key={name+'_'+d.n}
                        onClick={() => setSelection(d)}
                        active={d === selection}
                    >
                        <Table.Cell
                            collapsing
                            negative={(!d.metric | d.metric >= 1.0)}
                            warning={!(!d.metric | d.metric >= 1.0) & (!d.crit1 | !d.crit2)}
                            positive={d.crit1 | d.crit2}
                        >
                            {d.n}
                        </Table.Cell>
                        <Table.Cell collapsing>{d.time}</Table.Cell>
                        <Table.Cell textAlign='right'><code>{d.output}</code></Table.Cell>
                    </Table.Row>
                ))}
                { page === lastPage ? (
                    [...Array(n-slice.length).keys()].map(i => (
                        <Table.Row
                            key={name+'_missing_'+i}
                            disabled
                        >
                            <Table.Cell collapsing>-</Table.Cell>
                            <Table.Cell collapsing>-</Table.Cell>
                            <Table.Cell textAlign='right'><code>-</code></Table.Cell>
                        </Table.Row>
                    ))
                ) : null }
            </Table.Body>
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan='1' textAlign='center'>
                        { selection ? (
                            <Button
                                circular
                                icon='close'
                                size='tiny'
                                onClick={() => setSelection(null)}
                            />
                        ) : null }
                    </Table.HeaderCell>
                    <Table.HeaderCell colSpan='2' textAlign='right'>
                        <span className='sans smalltxt'>
                            Page {page+1} of {lastPage+1}{' '}
                        </span>
                        <Button.Group icon size='tiny'>
                            <Button
                                disabled={page <= 0}
                                onClick={() => {
                                    if (page > 0) {
                                        setPage(page - 1)
                                    }
                                }}
                            >
                                <Icon name='arrow left' />
                            </Button>
                            <Button
                                disabled={page >= lastPage}
                                onClick={() => {
                                    if (page < lastPage) {
                                        setPage(page + 1)
                                    }
                                }}
                            >
                                <Icon name='arrow right' />
                            </Button>
                        </Button.Group>
                    </Table.HeaderCell>
                </Table.Row>
                { selection ? (
                    <Table.Row textAlign='left'>
                        <Table.HeaderCell colSpan='3'>
                            <List relaxed>
                                <List.Item>
                                    <List.Icon name='clock' verticalAlign='middle' />
                                    <List.Content>
                                        {selection.time} steps of running time
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon
                                        name={selection.crit1 ? 'check circle' : 'remove circle'}
                                        color={selection.crit1 ? 'green' : 'red'}
                                        verticalAlign='middle'
                                    />
                                    <List.Content>
                                        Found before trivial program?
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon
                                        name={selection.crit2 ? 'check circle' : 'remove circle'}
                                        color={selection.crit2 ? 'green' : 'red'}
                                        verticalAlign='middle'
                                    />
                                    <List.Content>
                                        Smaller than trivial program?
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon
                                        name={selection.metric && selection.metric < 1.0 ? 'check circle' : 'remove circle'}
                                        color={selection.metric && selection.metric < 1.0 ? 'green' : 'red'}
                                        verticalAlign='middle'
                                    />
                                    <List.Content>
                                        Smaller than its output?
                                    </List.Content>
                                </List.Item>
                            </List>
                            <Header as='h4'>Program</Header>
                            <pre>{family.replaceAll('n', selection.n)}</pre>
                            <Divider />
                            <Header as='h4'>Output</Header>
                            <code style={{overflowWrap: 'anywhere'}}>{selection.output}</code>
                        </Table.HeaderCell>
                    </Table.Row>
                ) : null }
            </Table.Footer>
        </Table>
    )
}

const PowersOfTwo = () => {
    return (
        <Segment vertical>
            <Container text>
                <Header as='h3'>
                    Powers of two <InlineMath math='2^{n}' />
                </Header>
                <PaginatedTable
                    family={family1}
                    data={family1_data}
                    n={10}
                    name='pows2'
                />
            </Container>
        </Segment>
    )
}

const Factorial = () => {
    return (
        <Segment vertical>
            <Container text>
                <Header as='h3'>
                    Factorial <InlineMath math='n!' />
                </Header>
                <PaginatedTable
                    family={family2}
                    data={family2_data}
                    n={10}
                    name='fact'
                />
            </Container>
        </Segment>
    )
}

const Exponentiation1 = () => {
    return (
        <Segment vertical>
            <Container text>
                <Header as='h3'>
                    Exponentiation <InlineMath math='n^n' />
                </Header>
                <PaginatedTable
                    family={family3}
                    data={family3_data}
                    n={10}
                    name='expt1'
                />
            </Container>
        </Segment>
    )
}

const Exponentiation2 = () => {
    return (
        <Segment vertical>
            <Container text>
                <Header as='h3'>
                    Exponentiation <InlineMath math='n^{2^n}' />
                </Header>
                <PaginatedTable
                    family={family4}
                    data={family4_data}
                    n={10}
                    name='expt2'
                />
            </Container>
        </Segment>
    )
}

const FourFamilies = () => (
    <Segment vertical>
        <Container text>
            <Header as='h3'>
                Four families of programs
            </Header>
            <p>
                Here are four families of programs such that,
                for some values of <InlineMath math='n' />,
                the programs produce an output string clearly bigger
                than their length.
            </p>
            <Message size='tiny'>
                <Message.Header as='h4'>Powers of two <InlineMath math='2^{n}' /></Message.Header>
                <pre>
                    {family1}
                </pre>
            </Message>
            <Message size='tiny'>
                <Message.Header as='h4'>Factorial <InlineMath math='n!' /></Message.Header>
                <pre>
                    {family2}
                </pre>
            </Message>
            <Message size='tiny'>
                <Message.Header as='h4'>Exponentiation <InlineMath math='n^n' /></Message.Header>
                <pre>
                    {family3}
                </pre>
            </Message>
            <Message size='tiny'>
                <Message.Header as='h4'>Exponentiation <InlineMath math='n^{2^n}' /></Message.Header>
                <pre>
                    {family4}
                </pre>
            </Message>
            <p>
                In the following sections we present tables with
                information about the execution of programs in these
                families.
            </p>
            <p>
                The tables show the value of <InlineMath math='n' />{' '}
                encoding a program, the running time as the number of steps
                perfurm during its execution and the output string obtained.
            </p>
            <p>
                By clicking on a row, more information about the program is
                displayed below the table, such as:
            </p>
            <List bulleted>
                <List.Item>Whether the program was found before the trivial program with same output</List.Item>
                <List.Item>Whether the program improves upon the trivial upper bound for Kolmogorov complexity</List.Item>
                <List.Item>Whether the program is shorter than it's output string</List.Item>
                <List.Item>The textual representation of the program</List.Item>
                <List.Item>The full output string</List.Item>
            </List>
        </Container>
    </Segment>
)

const family1 = `
(x[0] := 1;
 (while (x[1] < n) do
   (x[1] := (x[1] + 1);
    x[0] := (x[0] * 2))))
`.trim()

const family2 = `
(x[0] := 1;
 (while (x[1] < n) do
   (x[1] := (x[1] + 1);
    x[0] := (x[0] * x[1]))))
`.trim()

const family3 = `
(x[0] := 1;
 (while (x[1] < n) do
   (x[1] := (x[1] + 1);
    x[0] := (x[0] * n))))
`.trim()

const family4 = `
(x[0] := n;
 (while (x[1] < n) do
   (x[1] := (x[1] + 1);
    x[0] := (x[0] * x[0]))))
`.trim()

const family1_data = [
    { 'n': 0, 'time': 7, 'output': '0', 'crit1': false, 'crit2': false, 'metric': 25.00 },
    { 'n': 1, 'time': 19, 'output': '10', 'crit1': false, 'crit2': false, 'metric': 12.50 },
    { 'n': 2, 'time': 31, 'output': '011', 'crit1': false, 'crit2': false, 'metric': 8.33 },
    { 'n': 3, 'time': 43, 'output': '00100', 'crit1': false, 'crit2': false, 'metric': 5.00 },
    { 'n': 4, 'time': 55, 'output': '000101', 'crit1': false, 'crit2': false, 'metric': 4.17 },
    { 'n': 5, 'time': 67, 'output': '0000110', 'crit1': false, 'crit2': false, 'metric': 3.57 },
    { 'n': 6, 'time': 79, 'output': '00000111', 'crit1': false, 'crit2': false, 'metric': 3.13 },
    { 'n': 7, 'time': 91, 'output': '0000001000', 'crit1': false, 'crit2': false, 'metric': 2.50 },
    { 'n': 8, 'time': 103, 'output': '00000001001', 'crit1': false, 'crit2': false, 'metric': 2.27 },
    { 'n': 9, 'time': 115, 'output': '000000001010', 'crit1': false, 'crit2': false, 'metric': 2.08 },
    { 'n': 10, 'time': 127, 'output': '0000000001011', 'crit1': false, 'crit2': false, 'metric': 2.00 },
    { 'n': 11, 'time': 139, 'output': '00000000001100', 'crit1': false, 'crit2': false, 'metric': 1.86 },
    { 'n': 12, 'time': 151, 'output': '000000000001101', 'crit1': false, 'crit2': false, 'metric': 1.73 },
    { 'n': 13, 'time': 163, 'output': '0000000000001110', 'crit1': false, 'crit2': false, 'metric': 1.63 },
    { 'n': 14, 'time': 175, 'output': '00000000000001111', 'crit1': false, 'crit2': false, 'metric': 1.53 },
    { 'n': 15, 'time': 187, 'output': '0000000000000010000', 'crit1': false, 'crit2': false, 'metric': 1.37 },
    { 'n': 16, 'time': 199, 'output': '00000000000000010001', 'crit1': false, 'crit2': false, 'metric': 1.30 },
    { 'n': 17, 'time': 211, 'output': '000000000000000010010', 'crit1': false, 'crit2': false, 'metric': 1.24 },
    { 'n': 18, 'time': 223, 'output': '0000000000000000010011', 'crit1': false, 'crit2': false, 'metric': 1.18 },
    { 'n': 19, 'time': 235, 'output': '00000000000000000010100', 'crit1': false, 'crit2': false, 'metric': 1.13 },
    { 'n': 20, 'time': 247, 'output': '000000000000000000010101', 'crit1': false, 'crit2': false, 'metric': 1.08 },
    { 'n': 21, 'time': 259, 'output': '0000000000000000000010110', 'crit1': false, 'crit2': false, 'metric': 1.04 },
    { 'n': 22, 'time': 271, 'output': '00000000000000000000010111', 'crit1': false, 'crit2': false, 'metric': 1.00 },
    { 'n': 23, 'time': 283, 'output': '000000000000000000000011000', 'crit1': false, 'crit2': false, 'metric': 0.96 },
    { 'n': 24, 'time': 295, 'output': '0000000000000000000000011001', 'crit1': false, 'crit2': false, 'metric': 0.93 },
    { 'n': 25, 'time': 307, 'output': '00000000000000000000000011010', 'crit1': false, 'crit2': false, 'metric': 0.90 },
    { 'n': 26, 'time': 319, 'output': '000000000000000000000000011011', 'crit1': false, 'crit2': false, 'metric': 0.87 },
    { 'n': 27, 'time': 331, 'output': '0000000000000000000000000011100', 'crit1': false, 'crit2': false, 'metric': 0.84 },
    { 'n': 28, 'time': 343, 'output': '00000000000000000000000000011101', 'crit1': false, 'crit2': false, 'metric': 0.81 },
    { 'n': 29, 'time': 355, 'output': '000000000000000000000000000011110', 'crit1': false, 'crit2': false, 'metric': 0.79 },
    { 'n': 30, 'time': 367, 'output': '0000000000000000000000000000011111', 'crit1': false, 'crit2': false, 'metric': 0.76 },
    { 'n': 31, 'time': 379, 'output': '000000000000000000000000000000100000', 'crit1': false, 'crit2': false, 'metric': 0.72 },
    { 'n': 32, 'time': 391, 'output': '0000000000000000000000000000000100001', 'crit1': false, 'crit2': false, 'metric': 0.70 },
    { 'n': 33, 'time': 403, 'output': '00000000000000000000000000000000100010', 'crit1': false, 'crit2': false, 'metric': 0.68 },
    { 'n': 34, 'time': 415, 'output': '000000000000000000000000000000000100011', 'crit1': false, 'crit2': false, 'metric': 0.67 },
    { 'n': 35, 'time': 427, 'output': '0000000000000000000000000000000000100100', 'crit1': false, 'crit2': false, 'metric': 0.65 },
    { 'n': 36, 'time': 439, 'output': '00000000000000000000000000000000000100101', 'crit1': false, 'crit2': false, 'metric': 0.63 },
    { 'n': 37, 'time': 451, 'output': '000000000000000000000000000000000000100110', 'crit1': false, 'crit2': false, 'metric': 0.62 },
    { 'n': 38, 'time': 463, 'output': '0000000000000000000000000000000000000100111', 'crit1': false, 'crit2': false, 'metric': 0.60 },
    { 'n': 39, 'time': 475, 'output': '00000000000000000000000000000000000000101000', 'crit1': false, 'crit2': false, 'metric': 0.59 },
    { 'n': 40, 'time': 487, 'output': '000000000000000000000000000000000000000101001', 'crit1': false, 'crit2': false, 'metric': 0.58 },
    { 'n': 41, 'time': 499, 'output': '0000000000000000000000000000000000000000101010', 'crit1': false, 'crit2': false, 'metric': 0.57 },
    { 'n': 42, 'time': 511, 'output': '00000000000000000000000000000000000000000101011', 'crit1': false, 'crit2': false, 'metric': 0.55 },
    { 'n': 43, 'time': 523, 'output': '000000000000000000000000000000000000000000101100', 'crit1': false, 'crit2': false, 'metric': 0.54 },
    { 'n': 44, 'time': 535, 'output': '0000000000000000000000000000000000000000000101101', 'crit1': false, 'crit2': false, 'metric': 0.53 },
    { 'n': 45, 'time': 547, 'output': '00000000000000000000000000000000000000000000101110', 'crit1': false, 'crit2': false, 'metric': 0.52 },
    { 'n': 46, 'time': 559, 'output': '000000000000000000000000000000000000000000000101111', 'crit1': false, 'crit2': false, 'metric': 0.51 },
    { 'n': 47, 'time': 571, 'output': '0000000000000000000000000000000000000000000000110000', 'crit1': false, 'crit2': false, 'metric': 0.50 },
    { 'n': 48, 'time': 583, 'output': '00000000000000000000000000000000000000000000000110001', 'crit1': false, 'crit2': false, 'metric': 0.49 },
    { 'n': 49, 'time': 595, 'output': '000000000000000000000000000000000000000000000000110010', 'crit1': false, 'crit2': false, 'metric': 0.48 },
    { 'n': 50, 'time': 607, 'output': '0000000000000000000000000000000000000000000000000110011', 'crit1': false, 'crit2': false, 'metric': 0.47 },
    { 'n': 51, 'time': 619, 'output': '00000000000000000000000000000000000000000000000000110100', 'crit1': false, 'crit2': false, 'metric': 0.46 },
    { 'n': 52, 'time': 631, 'output': '000000000000000000000000000000000000000000000000000110101', 'crit1': false, 'crit2': false, 'metric': 0.46 },
    { 'n': 53, 'time': 643, 'output': '0000000000000000000000000000000000000000000000000000110110', 'crit1': false, 'crit2': false, 'metric': 0.45 },
    { 'n': 54, 'time': 655, 'output': '00000000000000000000000000000000000000000000000000000110111', 'crit1': false, 'crit2': false, 'metric': 0.44 },
    { 'n': 55, 'time': 667, 'output': '000000000000000000000000000000000000000000000000000000111000', 'crit1': false, 'crit2': false, 'metric': 0.43 },
    { 'n': 56, 'time': 679, 'output': '0000000000000000000000000000000000000000000000000000000111001', 'crit1': false, 'crit2': false, 'metric': 0.43 },
    { 'n': 57, 'time': 691, 'output': '00000000000000000000000000000000000000000000000000000000111010', 'crit1': false, 'crit2': false, 'metric': 0.42 },
    { 'n': 58, 'time': 703, 'output': '000000000000000000000000000000000000000000000000000000000111011', 'crit1': false, 'crit2': false, 'metric': 0.41 },
    { 'n': 59, 'time': 715, 'output': '0000000000000000000000000000000000000000000000000000000000111100', 'crit1': false, 'crit2': false, 'metric': 0.41 },
    { 'n': 60, 'time': 727, 'output': '00000000000000000000000000000000000000000000000000000000000111101', 'crit1': false, 'crit2': false, 'metric': 0.40 },
    { 'n': 61, 'time': 739, 'output': '000000000000000000000000000000000000000000000000000000000000111110', 'crit1': false, 'crit2': false, 'metric': 0.39 },
    { 'n': 62, 'time': 751, 'output': '0000000000000000000000000000000000000000000000000000000000000111111', 'crit1': false, 'crit2': false, 'metric': 0.39 },
    { 'n': 63, 'time': 763, 'output': '000000000000000000000000000000000000000000000000000000000000001000000', 'crit1': false, 'crit2': false, 'metric': 0.38 },
    { 'n': 64, 'time': 775, 'output': '0000000000000000000000000000000000000000000000000000000000000001000001', 'crit1': false, 'crit2': false, 'metric': 0.37 },
    { 'n': 65, 'time': 787, 'output': '00000000000000000000000000000000000000000000000000000000000000001000010', 'crit1': false, 'crit2': false, 'metric': 0.37 },
    { 'n': 66, 'time': 799, 'output': '000000000000000000000000000000000000000000000000000000000000000001000011', 'crit1': false, 'crit2': false, 'metric': 0.36 },
    { 'n': 67, 'time': 811, 'output': '0000000000000000000000000000000000000000000000000000000000000000001000100', 'crit1': false, 'crit2': false, 'metric': 0.36 },
    { 'n': 68, 'time': 823, 'output': '00000000000000000000000000000000000000000000000000000000000000000001000101', 'crit1': false, 'crit2': false, 'metric': 0.35 },
    { 'n': 69, 'time': 835, 'output': '000000000000000000000000000000000000000000000000000000000000000000001000110', 'crit1': false, 'crit2': false, 'metric': 0.35 },
    { 'n': 70, 'time': 847, 'output': '0000000000000000000000000000000000000000000000000000000000000000000001000111', 'crit1': false, 'crit2': false, 'metric': 0.34 },
    { 'n': 71, 'time': 859, 'output': '00000000000000000000000000000000000000000000000000000000000000000000001001000', 'crit1': true, 'crit2': true, 'metric': 0.34 },
    { 'n': 72, 'time': 871, 'output': '000000000000000000000000000000000000000000000000000000000000000000000001001001', 'crit1': true, 'crit2': true, 'metric': 0.33 },
    { 'n': 73, 'time': 883, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000001001010', 'crit1': true, 'crit2': true, 'metric': 0.33 },
    { 'n': 74, 'time': 895, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000001001011', 'crit1': true, 'crit2': true, 'metric': 0.33 },
    { 'n': 75, 'time': 907, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000001001100', 'crit1': true, 'crit2': true, 'metric': 0.32 },
    { 'n': 76, 'time': 919, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000001001101', 'crit1': true, 'crit2': true, 'metric': 0.32 },
    { 'n': 77, 'time': 931, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000001001110', 'crit1': true, 'crit2': true, 'metric': 0.31 },
    { 'n': 78, 'time': 943, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000001001111', 'crit1': true, 'crit2': true, 'metric': 0.31 },
    { 'n': 79, 'time': 955, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000001010000', 'crit1': true, 'crit2': true, 'metric': 0.31 },
    { 'n': 80, 'time': 967, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000001010001', 'crit1': true, 'crit2': true, 'metric': 0.30 },
    { 'n': 81, 'time': 979, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000001010010', 'crit1': true, 'crit2': true, 'metric': 0.30 },
    { 'n': 82, 'time': 991, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000001010011', 'crit1': true, 'crit2': true, 'metric': 0.30 },
    { 'n': 83, 'time': 1003, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000001010100', 'crit1': true, 'crit2': true, 'metric': 0.29 },
    { 'n': 84, 'time': 1015, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000001010101', 'crit1': true, 'crit2': true, 'metric': 0.29 },
    { 'n': 85, 'time': 1027, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000001010110', 'crit1': true, 'crit2': true, 'metric': 0.29 },
    { 'n': 86, 'time': 1039, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000000001010111', 'crit1': true, 'crit2': true, 'metric': 0.28 },
    { 'n': 87, 'time': 1051, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011000', 'crit1': true, 'crit2': true, 'metric': 0.28 },
    { 'n': 88, 'time': 1063, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011001', 'crit1': true, 'crit2': true, 'metric': 0.28 },
    { 'n': 89, 'time': 1075, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011010', 'crit1': true, 'crit2': true, 'metric': 0.27 },
    { 'n': 90, 'time': 1087, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011011', 'crit1': true, 'crit2': true, 'metric': 0.27 },
    { 'n': 91, 'time': 1099, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011100', 'crit1': true, 'crit2': true, 'metric': 0.27 },
    { 'n': 92, 'time': 1111, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011101', 'crit1': true, 'crit2': true, 'metric': 0.27 },
    { 'n': 93, 'time': 1123, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011110', 'crit1': true, 'crit2': true, 'metric': 0.26 },
    { 'n': 94, 'time': 1135, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011111', 'crit1': true, 'crit2': true, 'metric': 0.26 },
    { 'n': 95, 'time': 1147, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100000', 'crit1': true, 'crit2': true, 'metric': 0.26 },
    { 'n': 96, 'time': 1159, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100001', 'crit1': true, 'crit2': true, 'metric': 0.25 },
    { 'n': 97, 'time': 1171, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100010', 'crit1': true, 'crit2': true, 'metric': 0.25 },
    { 'n': 98, 'time': 1183, 'output': '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100011', 'crit1': true, 'crit2': true, 'metric': 0.25 },
    { 'n': 99, 'time': 1195, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100100', 'crit1': true, 'crit2': true, 'metric': 0.25 },
    { 'n': 100, 'time': 1207, 'output': '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001100101', 'crit1': true, 'crit2': true, 'metric': 0.25 },
]


const family2_data = [
    { 'n': 0, 'time': 7, 'output': '0', 'crit1': false, 'crit2': false, 'metric': 26.00 },
    { 'n': 1, 'time': 19, 'output': '00', 'crit1': false, 'crit2': false, 'metric': 13.00 },
    { 'n': 2, 'time': 31, 'output': '11', 'crit1': false, 'crit2': false, 'metric': 13.00 },
    { 'n': 3, 'time': 43, 'output': '1100', 'crit1': false, 'crit2': false, 'metric': 6.50 },
    { 'n': 4, 'time': 55, 'output': '100101', 'crit1': false, 'crit2': false, 'metric': 4.33 },
    { 'n': 5, 'time': 67, 'output': '11100110', 'crit1': false, 'crit2': false, 'metric': 3.25 },
    { 'n': 6, 'time': 79, 'output': '01101000111', 'crit1': false, 'crit2': false, 'metric': 2.36 },
    { 'n': 7, 'time': 91, 'output': '001110110001000', 'crit1': false, 'crit2': false, 'metric': 1.73 },
    { 'n': 8, 'time': 103, 'output': '001110110000001001', 'crit1': false, 'crit2': false, 'metric': 1.44 },
    { 'n': 9, 'time': 115, 'output': '011000100110000001010', 'crit1': false, 'crit2': false, 'metric': 1.24 },
    { 'n': 10, 'time': 127, 'output': '101110101111100000001011', 'crit1': false, 'crit2': false, 'metric': 1.13 },
    { 'n': 11, 'time': 139, 'output': '0011000010001010100000001100', 'crit1': false, 'crit2': false, 'metric': 0.96 },
    { 'n': 12, 'time': 151, 'output': '1100100011001111110000000001101', 'crit1': false, 'crit2': false, 'metric': 0.87 },
    { 'n': 13, 'time': 163, 'output': '01110011001010001100110000000001110', 'crit1': false, 'crit2': false, 'metric': 0.77 },
    { 'n': 14, 'time': 175, 'output': '010001001100001110110010100000000001111', 'crit1': false, 'crit2': false, 'metric': 0.69 },
    { 'n': 15, 'time': 187, 'output': '00110000011101110111011101011000000000010000', 'crit1': false, 'crit2': false, 'metric': 0.61 },
    { 'n': 16, 'time': 199, 'output': '001100000111011101110111010110000000000000010001', 'crit1': false, 'crit2': false, 'metric': 0.56 },
    { 'n': 17, 'time': 211, 'output': '0100001101111110111011101100110110000000000000010010', 'crit1': false, 'crit2': false, 'metric': 0.52 },
    { 'n': 18, 'time': 223, 'output': '01101011111011101100110010100111001100000000000000010011', 'crit1': false, 'crit2': false, 'metric': 0.48 },
    { 'n': 19, 'time': 235, 'output': '101100000010101110010011000001101000100100000000000000010100', 'crit1': false, 'crit2': false, 'metric': 0.45 },
    { 'n': 20, 'time': 247, 'output': '00001110000110110011101111100100000101011010000000000000000010101', 'crit1': false, 'crit2': false, 'metric': 0.42 },
    { 'n': 21, 'time': 259, 'output': '011000101000001110111110100110110101110001100010000000000000000010110', 'crit1': false, 'crit2': false, 'metric': 0.39 },
    { 'n': 22, 'time': 271, 'output': '1110011101110101001001100001010110011111000001101100000000000000000010111', 'crit1': true, 'crit2': false, 'metric': 0.37 },
    { 'n': 23, 'time': 283, 'output': '010111100101110000110011010111111000101001001100110110100000000000000000011000', 'crit1': true, 'crit2': false, 'metric': 0.35 },
    { 'n': 24, 'time': 295, 'output': '00000110110001010010011010000111101001111011100110100011100000000000000000000011001', 'crit1': true, 'crit2': true, 'metric': 0.33 },
    { 'n': 25, 'time': 307, 'output': '100110101001010000001100001100111111011000010010000011110111100000000000000000000011010', 'crit1': true, 'crit2': true, 'metric': 0.31 },
    { 'n': 26, 'time': 319, 'output': '01001101100110000100100111101010001101111110111010101100100100011000000000000000000000011011', 'crit1': true, 'crit2': true, 'metric': 0.29 },
    { 'n': 27, 'time': 331, 'output': '0001100101111000011111100101110110011111001100010110000110011010110001000000000000000000000011100', 'crit1': true, 'crit2': true, 'metric': 0.28 },
    { 'n': 28, 'time': 343, 'output': '11101100100100101101110100100011110101101001011001101010110011101101011100000000000000000000000011101', 'crit1': true, 'crit2': true, 'metric': 0.27 },
    { 'n': 29, 'time': 355, 'output': '1011111001100101000110000110100001111010011110000101000011001011011100101101100000000000000000000000011110', 'crit1': true, 'crit2': true, 'metric': 0.25 },
    { 'n': 30, 'time': 367, 'output': '101000100111111011000110111000011111001011010000110010111011111010111011101010101000000000000000000000000011111', 'crit1': true, 'crit2': true, 'metric': 0.24 },
    { 'n': 31, 'time': 379, 'output': '100101010110101011010000101010101110001100111010010001010110000011000101110011010010110000000000000000000000000100000', 'crit1': true, 'crit2': true, 'metric': 0.23 },
    { 'n': 32, 'time': 391, 'output': '10010101011010101101000010101010111000110011101001000101011000001100010111001101001011000000000000000000000000000000100001', 'crit1': true, 'crit2': true, 'metric': 0.22 },
]

const family3_data = [
    { 'n': 0, 'time': 7, 'output': '0', 'crit1': false, 'crit2': false, 'metric': 25.00 },
    { 'n': 1, 'time': 19, 'output': '00', 'crit1': false, 'crit2': false, 'metric': 12.50 },
    { 'n': 2, 'time': 31, 'output': '011', 'crit1': false, 'crit2': false, 'metric': 8.33 },
    { 'n': 3, 'time': 43, 'output': '110000', 'crit1': false, 'crit2': false, 'metric': 4.17 },
    { 'n': 4, 'time': 55, 'output': '0000000101', 'crit1': false, 'crit2': false, 'metric': 2.50 },
    { 'n': 5, 'time': 67, 'output': '1000011011010', 'crit1': false, 'crit2': false, 'metric': 1.92 },
    { 'n': 6, 'time': 79, 'output': '01101100100000111', 'crit1': false, 'crit2': false, 'metric': 1.47 },
    { 'n': 7, 'time': 91, 'output': '1001001000011111000000', 'crit1': false, 'crit2': false, 'metric': 1.14 },
    { 'n': 8, 'time': 103, 'output': '000000000000000000000001001', 'crit1': false, 'crit2': false, 'metric': 0.93 },
    { 'n': 9, 'time': 115, 'output': '0111000101111001000101001010010', 'crit1': false, 'crit2': false, 'metric': 0.81 },
    { 'n': 10, 'time': 127, 'output': '001010100000010111110010000000001011', 'crit1': false, 'crit2': false, 'metric': 0.75 },
    { 'n': 11, 'time': 139, 'output': '00001001101101111001101001100101010100100', 'crit1': false, 'crit2': false, 'metric': 0.66 },
    { 'n': 12, 'time': 151, 'output': '0000001101111110001000000000000000000000001101', 'crit1': false, 'crit2': false, 'metric': 0.59 },
    { 'n': 13, 'time': 163, 'output': '000100110111011010011011001000111100010111111110110', 'crit1': false, 'crit2': false, 'metric': 0.53 },
    { 'n': 14, 'time': 175, 'output': '00111011110100100111110110011100101000100000000000001111', 'crit1': false, 'crit2': false, 'metric': 0.48 },
    { 'n': 15, 'time': 187, 'output': '10000100111011011000101100010110010111011100000111111100000000', 'crit1': false, 'crit2': false, 'metric': 0.44 },
    { 'n': 16, 'time': 199, 'output': '00000000000000000000000000000000000000000000000000000000000000010001', 'crit1': false, 'crit2': false, 'metric': 0.40 },
    { 'n': 17, 'time': 211, 'output': '0110011011000010000111100101101000111011001000011011100001001000100100010', 'crit1': false, 'crit2': false, 'metric': 0.37 },
    { 'n': 18, 'time': 223, 'output': '0000101010011111001000110100010111001000111000110110100010000000000000000010011', 'crit1': false, 'crit2': false, 'metric': 0.34 },
    { 'n': 19, 'time': 235, 'output': '101000101111001001011000001011010001001001111001111011011011100100001011100011000100', 'crit1': false, 'crit2': true, 'metric': 0.32 },
    { 'n': 20, 'time': 247, 'output': '010110101111000111010111100010110101100011000100000000000000000000000000000000000000010101', 'crit1': false, 'crit2': true, 'metric': 0.30 },
    { 'n': 21, 'time': 259, 'output': '001011100000110111111110110011101001110110100001110100101010001101000101011001111001010001100110', 'crit1': false, 'crit2': true, 'metric': 0.28 },
    { 'n': 22, 'time': 271, 'output': '000100111100110110100001010100111101000101001000101011111000010100001110100100000000000000000000010111', 'crit1': false, 'crit2': true, 'metric': 0.26 },
    { 'n': 23, 'time': 283, 'output': '000001111000110001101110010011110111110101110101010001010000101100011111101100111110110001101010111010001000', 'crit1': false, 'crit2': true, 'metric': 0.25 },
    { 'n': 24, 'time': 295, 'output': '000001110000100001110010111000111000010000000000000000000000000000000000000000000000000000000000000000000000011001', 'crit1': false, 'crit2': true, 'metric': 0.24 },
    { 'n': 25, 'time': 307, 'output': '000100011011000011101100010101111110011001001001100110100001111101001011000100000001010011010011111101101101010110101010', 'crit1': false, 'crit2': true, 'metric': 0.23 },
    { 'n': 26, 'time': 319, 'output': '001010000110100000001000010000100011111001011100011010011111110000011010010011010101110000001001000000000000000000000000011011', 'crit1': false, 'crit2': true, 'metric': 0.21 },
    { 'n': 27, 'time': 331, 'output': '010011011001100011010101110011101010000101001001111010000011010010110110101111110000110001101001110101010110110101111100110001001100', 'crit1': true, 'crit2': true, 'metric': 0.20 },
    { 'n': 28, 'time': 343, 'output': '100001011001111110101110001100011100100101001000000111100000100111010110100001000000000000000000000000000000000000000000000000000000011101', 'crit1': true, 'crit2': true, 'metric': 0.20 },
    { 'n': 29, 'time': 355, 'output': '110101111001110000000101110100000100001000110101111010001000000001111100001101001100101111000011011010101000101101001000101001001100000011101110', 'crit1': true, 'crit2': true, 'metric': 0.19 },
    { 'n': 30, 'time': 367, 'output': '0010011101110000011101010000001101001111100101110111010000000111100001000100100000100100011100111000011110001001000010000000000000000000000000000011111', 'crit1': true, 'crit2': true, 'metric': 0.18 },
]

const family4_data = [
    { 'n': 0, 'time': 7, 'output': '', 'crit1': false, 'crit2': false, 'metric': null },
    { 'n': 1, 'time': 19, 'output': '00', 'crit1': false, 'crit2': false, 'metric': 13.00 },
    { 'n': 2, 'time': 31, 'output': '00011', 'crit1': false, 'crit2': false, 'metric': 5.20 },
    { 'n': 3, 'time': 43, 'output': '10011010001000', 'crit1': false, 'crit2': false, 'metric': 1.86 },
    { 'n': 4, 'time': 55, 'output': '0000000000000000000000000000000101', 'crit1': false, 'crit2': false, 'metric': 0.76 },
    { 'n': 5, 'time': 67, 'output': '0011101110001011010110110101000001010110111000010110101100111011111000001010', 'crit1': true, 'crit2': false, 'metric': 0.34 },
    { 'n': 6, 'time': 79, 'output': '01011010101101101010010101111100011110111100100110010001001111000110001111001011111101011110100000001000000000000000000000000000000000000000000000000000000000000000111', 'crit1': true, 'crit2': true, 'metric': 0.16 },
    { 'n': 7, 'time': 91, 'output': '01000100010110101110001110010000111101010011100111001000000011011011010010011110101100010010101111110011111001111001100001111010110000000110001100100101011101011110110110010001010001010110101110000110010011001001001110100000100011001011011111100011000100011111110011110011001101100110110000110000000100001011110010010011101011100011011110110101110110000000010000', 'crit1': true, 'crit2': true, 'metric': 0.07 },
    { 'n': 8, 'time': 103, 'output': '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001001', 'crit1': true, 'crit2': true, 'metric': 0.03 },
    { 'n': 9, 'time': 115, 'output': '000000000100100011000001011100100101010001111100010001101011101010001101001010010010001111100010110110101010100100001100010010000101110101011010011100011100001100110000111110111111100111100110100010011111101110000101111101101100010100000010001111011000011011011001110001101110010000000101111001110101111000110111000000001110000111010000000000011001011001001011100100100001111001110101001111100001000010011111100010100111010000011010110010110101100001111111001000111001110100111111101100010101100110111000110111101100011110100001001100101011001100101110000001010011000111111100011110000010101110001000110111100110010101101110010000010000101101000110000101111101011100111011111011010111010111101111001001100100001110000101000111101101101010011100000100100000111000001010101001110110001001100001010100011010101100110101001100100101100101001001000011011100011010001100010100101110111110000111110100101101000111100000000111100001110010100110111011001010110100101100001000000010011100100110011001101100110010111000011010111011001011010010000111010011010100111110000111101000101101111101011111010100100010011011110101001000101010111000010111110100000010110100111100001010000101100000000011111101001000110100101100000010101101010110000110011100100000101101110000101100110111000010001100110001100001011000110001000110010010011010000110000100001011000110010100111101001100000100000000010011100101001010000101011010110001010111001010110001110011100000000111001110001101110011111100110100101110110011010111101001001001110100100010110101100000100010101111101001010110101101001110110100100101000001001101110010000001000111101000000000010010', 'crit1': true, 'crit2': true, 'metric': 0.02 },
    { 'n': 10, 'time': 127, 'output': '10010010111011001110101100001101000000101110101000011000001011101100101000011010011110100101000111100011000101101010000110011100101001001101011100110001000001001110101001001001001101100001111110101101111010010110110110100100111101111010001110001100001110000100101001100000110100110100101110000110010100111111001101011110100101010011100111100010101010011010010010011011010101000110111100000011000011110100011010100011001001001111101001111001100100011010111100000000001011011100000000100000100010110010000000001000101001010100010001011110001001001000110111001111111000100111011010101001000000001100100001111101110100000001101011111101010101011101001101100001101010110111000111000101000110001101101010110010100111111010001000101100101110101100000110110110010110000101101101100100101110101101000110101000110100010100011100100001001100010110011100011110011100011101000001011100011011111011010000011011010001010111000100110101001111011011111101001011101101010001100110001000011111100100011110001111110010000001100011110101100001111011001101001101000100000001010001000001010000000101111011010010110100101100000001101100110000000100010011101110000011010010001111111111001101011011101011001010111111011000100000001101000110110000101101011011011110100011001010100101100110001111110110110101111111101111101010110110111101110111011001011000110111000100101101011110010001111110001010001101000010110101111110011111111011110101100111101100000010100101101010000010110111010001101001100010000010001100010101101100000100010101001010001101010111110000111011110001100000100010110110111101001110100100011100001000001000010110001000110001010111110011010010101101010011111100111110010101000000111101001111110100010010100001100011001001000101011101110111001111111001001011000110101100110000110000111000101110011010001001101001111010001010001001000011000101101100101110111000110000011010111001000110011111001010101110001110010001100011101001010111001110010011111110011101000001101111101000111100100001111110110000111110111110111100001011011001111101110001001011001100011010011101110000011101101000101000100000011101100110000101100110110000001100101001100011111010001111000011010000101000000101011000101001101010001010111000101000110101000100010011001000111000100111010000111000111101100001010011111100001100110111001111100001000010101101111110101010110110010101101110010001010010001100111110000000000010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001011', 'crit1': true, 'crit2': true, 'metric': 0.01 },
]
