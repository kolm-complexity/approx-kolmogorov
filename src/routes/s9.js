import React, { useState } from 'react'
import { InlineMath } from 'react-katex'
import {
    Button,
    Container,
    Header,
    Segment,
    Table,
    Icon,
    Form,
    Message,
} from 'semantic-ui-react'
import { Chart } from '../components/Chart'

import rtime_log_sample_01 from '../assets/rtime-log-sample-01.png'
import output_sample_01 from '../assets/output-sample-01.png'
import length_rtime_log from '../assets/length-rtime-log.png'
import output_length_log from '../assets/output-length-log.png'
import length_rtime_log_11 from '../assets/length-rtime-log-11.png'
import output_length_log_11 from '../assets/output-length-log-11.png'

const SortedExperiment = () => {
    return (
        <main>
            <Segment
                inverted
                textAlign='center'
                padded
            >
                <Container text>
                    <Header as='h2' inverted>
                        Sampling and executing programs in{' '}
                        <InlineMath math={`\\mathcal{S}_9`} />
                    </Header>
                </Container>
            </Segment>
            <SampleS9 />
            <FullS9 />
            <PreliminaryS11 />
            <TrivialUpperBoundCalculator />
        </main>
    )
}

export default SortedExperiment

const TrivialUpperBoundCalculator = () => {
    const [output, setOutput] = useState('')
    const [program, setProgram] = useState(null)
    return (
        <Segment vertical>
            <Container text>
                <Header as='h3'>
                    Trivial complexity upper-bound calculator
                </Header>
                <p>
                    A trivial upper bound for Kolmogorov complexity can
                    be easily calculated by measuring the length of a
                    program that performes one assignment to the first
                    register such that the output string is obtained.
                </p>
                <p>
                    In the following form, by entering a binary string
                    in the input field and clicking on <em>Generate</em>{' '}
                    the trivial program and the trivial complexity upper bound
                    are shown.
                </p>
                <Form>
                    <Form.Input
                        fluid
                        label='Output string'
                        placeholder='Binary digits'
                        className='sans ttinput'
                        value={output}
                        onChange={e => {
                            if (!e.target.value.match('[^01]')) {
                                setOutput(e.target.value)
                            }
                        }}
                    />
                    <Button
                        onClick={() => {
                            if (output === '') {
                                setProgram('skip')
                            } else {
                                const l = output.length
                                const c = parseInt(output, 2)
                                const b = (2**l) - 1
                                const n = b + c
                                setProgram('x[0] := '+n)
                            }
                        }}
                    >
                        Generate
                    </Button>
                </Form>
                {program ? (
                    <Segment basic>
                        <Message info attached='top'>
                            <Message.Header>Trivial program</Message.Header>
                            <code>
                                {program}
                            </code>
                        </Message>
                        <Message info attached='bottom'>
                            <Message.Header>Complexity upper bound</Message.Header>
                            <code>
                                {program === 'skip' ? 0 : (
                                    program.split(' ')[2].length + 3
                                )}
                            </code>
                        </Message>
                    </Segment>
                ) : null}
            </Container>
        </Segment>
    )
}

const SampleS9 = () => (
    <Segment vertical>
        <Container text>
            <Header as='h3'>
                Sample of <InlineMath math={'N'} /> programs
            </Header>
            <p>
                Setting a sampling size of <InlineMath math='N = 13815511' />,
                a time limit of <InlineMath math='\mathbf{T}=9' /> steps was
                obtained using Calude et al approach.
            </p>
            <p>
                The value was validated running the sampled programs of
                size 9 or less upto <InlineMath math='10^4' /> steps.
                The following figure shows the distribution of the sample
                by running time
            </p>
        </Container>
        <Chart src={rtime_log_sample_01} size='medium' />
        <Container text>
            <p>
                The following figure shows how the lengths of the outputs
                in the sample are distributed, the empty string is the
                most common output and the largest output has size 9.
            </p>
        </Container>
        <Chart src={output_sample_01} size='medium' />
    </Segment>
)

const FullS9 = () => (
    <Segment vertical>
        <Container text>
            <Header as='h3'>
                Systematic execution of programs in <InlineMath math={'\\mathcal{S}_9'} />
            </Header>
            <p>
                The following table classifies the number of programs
                by length into halting and non-halting, along with
                the percentage of the total count.
            </p>
        </Container>
        <Table
            celled
            selectable
            compact='very'
            collapsing
            textAlign='right'
            className='force-center'
        >
            <Table.Header className='sans'>
                <Table.Row>
                    {sample_table_01.headers.map((x, i) => (
                        <Table.HeaderCell key={x + i}>{x}</Table.HeaderCell>
                    ))}
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {sample_table_01.rows.map((xs, i) => (
                    <Table.Row key={'_' + i}>
                        {xs.map((x, j) => (
                            <Table.Cell key={'_' + i + '_' + j}>
                                {x}
                            </Table.Cell>
                        ))}
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>
        <Container text>
            <p>
                The following figure shows the distribution
                of halting-programs by program length and running time.
            </p>
        </Container>
        <Chart src={length_rtime_log} size='medium' />
        <Container text>
            <p>
                The following figure shows the distribution
                of halting-programs by output length and program length.
            </p>
        </Container>
        <Chart src={output_length_log} size='medium' />
        <Container text>
            <p>
                As the figures show, the programs explored were at most
                as large as their running time. And the ouput lengths
                follow a at most a linear relation with program length.
            </p>
            <p>
                In the following table, the smallest programs
                that produce the strings in the second column are
                presented.
            </p>
            <p>
                The length of this programs corresponds to the
                complexity of the associated strings.
            </p>
        </Container>
        <ProgramsLength8 />
        <Container text>
            <p>
                In this subset of IMP programs the smallest
                program found for all output strings obtained
                is a trivial program.
            </p>
        </Container>
    </Segment>
)

const PreliminaryS11 = () => (
    <Segment vertical>
        <Container text>
            <Header as='h3'>
                Preliminary results for <InlineMath math={'\\mathcal{S}_{11}'} />
            </Header>
            <p>
                Preliminary results for the execution of programs with length
                at most 11 are shown below.
            </p>
            <p>
                We are starting to see programs whose execution
                times exceed their length, and we think this region will grow
                faster in larger spaces. Longer execution times are needed to
                produce larger outputs.
            </p>
        </Container>
        <Chart src={length_rtime_log_11} size='medium' />
        <Chart src={output_length_log_11} size='medium' />
    </Segment>
)

const sample_table_01 = {
    headers: ['Length', 'Halt', '(%)', 'Non-halt', '(%)'],
    rows: [
        ['1', '1', '(100.0%)', '0', '(0.0%)'],
        ['3', '2', '(66.7%)', '1', '(33.3%)'],
        ['4', '103', '(99.0%)', '1', '(1.0%)'],
        ['5', '2059', '(96.9%)', '65', '(3.1%)'],
        ['6', '34491', '(96.4%)', '1279', '(3.6%)'],
        ['7', '522060', '(95.5%)', '24551', '(4.5%)'],
        ['8', '7578748', '(94.8%)', '412428', '(5.2%)'],
        ['9', '107800381', '(94.1%)', '6713451', '(5.9%)'],
    ],
}

const ProgramsLength8 = () => {
    const n = 10
    const total = programs_length8.length
    const lastPage = Math.floor(total / n)
    const [page, setPage] = useState(0)
    const slice = programs_length8.slice(n*page, n*page+n)
    
    return (
        <Table
            celled
            selectable
            compact='very'
            collapsing
            textAlign='center'
            className='force-center'
        >
            <Table.Header className='sans'>
                <Table.Row>
                    <Table.HeaderCell colSpan='3'>Program</Table.HeaderCell>
                    <Table.HeaderCell rowSpan='2'>Output</Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                    <Table.HeaderCell>Length</Table.HeaderCell>
                    <Table.HeaderCell>Time</Table.HeaderCell>
                    <Table.HeaderCell>Code</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                {slice.map(p => (
                    <Table.Row key={p.output}>
                        <Table.Cell>{p.length}</Table.Cell>
                        <Table.Cell>{p.time}</Table.Cell>
                        <Table.Cell><code>{p.program}</code></Table.Cell>
                        <Table.Cell><code>{p.output}</code></Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan='4' textAlign='right'>
                        <span className='sans smalltxt'>
                            Page {page+1} of {lastPage+1}{' '}
                        </span>
                        <Button.Group icon size='tiny'>
                            <Button
                                disabled={page <= 0}
                                onClick={() => {
                                    if (page > 0) {
                                        setPage(page-1)
                                    }
                                }}
                            >
                                <Icon name='arrow left' />
                            </Button>
                            <Button
                                disabled={page >= lastPage}
                                onClick={() => {
                                    if (page < lastPage) {
                                        setPage(page+1)
                                    }
                                }}
                            >
                                <Icon name='arrow right' />
                            </Button>
                        </Button.Group>
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        </Table>
    )
}

const programs_length8 = [
    { 'length': 6, 'time': 2, 'output': '00000000', 'program': 'x[0] := 255' },
    { 'length': 6, 'time': 2, 'output': '00000001', 'program': 'x[0] := 256' },
    { 'length': 6, 'time': 2, 'output': '00000010', 'program': 'x[0] := 257' },
    { 'length': 6, 'time': 2, 'output': '00000011', 'program': 'x[0] := 258' },
    { 'length': 6, 'time': 2, 'output': '00000100', 'program': 'x[0] := 259' },
    { 'length': 6, 'time': 2, 'output': '00000101', 'program': 'x[0] := 260' },
    { 'length': 6, 'time': 2, 'output': '00000110', 'program': 'x[0] := 261' },
    { 'length': 6, 'time': 2, 'output': '00000111', 'program': 'x[0] := 262' },
    { 'length': 6, 'time': 2, 'output': '00001000', 'program': 'x[0] := 263' },
    { 'length': 6, 'time': 2, 'output': '00001001', 'program': 'x[0] := 264' },
    { 'length': 6, 'time': 2, 'output': '00001010', 'program': 'x[0] := 265' },
    { 'length': 6, 'time': 2, 'output': '00001011', 'program': 'x[0] := 266' },
    { 'length': 6, 'time': 2, 'output': '00001100', 'program': 'x[0] := 267' },
    { 'length': 6, 'time': 2, 'output': '00001101', 'program': 'x[0] := 268' },
    { 'length': 6, 'time': 2, 'output': '00001110', 'program': 'x[0] := 269' },
    { 'length': 6, 'time': 2, 'output': '00001111', 'program': 'x[0] := 270' },
    { 'length': 6, 'time': 2, 'output': '00010000', 'program': 'x[0] := 271' },
    { 'length': 6, 'time': 2, 'output': '00010001', 'program': 'x[0] := 272' },
    { 'length': 6, 'time': 2, 'output': '00010010', 'program': 'x[0] := 273' },
    { 'length': 6, 'time': 2, 'output': '00010011', 'program': 'x[0] := 274' },
    { 'length': 6, 'time': 2, 'output': '00010100', 'program': 'x[0] := 275' },
    { 'length': 6, 'time': 2, 'output': '00010101', 'program': 'x[0] := 276' },
    { 'length': 6, 'time': 2, 'output': '00010110', 'program': 'x[0] := 277' },
    { 'length': 6, 'time': 2, 'output': '00010111', 'program': 'x[0] := 278' },
    { 'length': 6, 'time': 2, 'output': '00011000', 'program': 'x[0] := 279' },
    { 'length': 6, 'time': 2, 'output': '00011001', 'program': 'x[0] := 280' },
    { 'length': 6, 'time': 2, 'output': '00011010', 'program': 'x[0] := 281' },
    { 'length': 6, 'time': 2, 'output': '00011011', 'program': 'x[0] := 282' },
    { 'length': 6, 'time': 2, 'output': '00011100', 'program': 'x[0] := 283' },
    { 'length': 6, 'time': 2, 'output': '00011101', 'program': 'x[0] := 284' },
    { 'length': 6, 'time': 2, 'output': '00011110', 'program': 'x[0] := 285' },
    { 'length': 6, 'time': 2, 'output': '00011111', 'program': 'x[0] := 286' },
    { 'length': 6, 'time': 2, 'output': '00100000', 'program': 'x[0] := 287' },
    { 'length': 6, 'time': 2, 'output': '00100001', 'program': 'x[0] := 288' },
    { 'length': 6, 'time': 2, 'output': '00100010', 'program': 'x[0] := 289' },
    { 'length': 6, 'time': 2, 'output': '00100011', 'program': 'x[0] := 290' },
    { 'length': 6, 'time': 2, 'output': '00100100', 'program': 'x[0] := 291' },
    { 'length': 6, 'time': 2, 'output': '00100101', 'program': 'x[0] := 292' },
    { 'length': 6, 'time': 2, 'output': '00100110', 'program': 'x[0] := 293' },
    { 'length': 6, 'time': 2, 'output': '00100111', 'program': 'x[0] := 294' },
    { 'length': 6, 'time': 2, 'output': '00101000', 'program': 'x[0] := 295' },
    { 'length': 6, 'time': 2, 'output': '00101001', 'program': 'x[0] := 296' },
    { 'length': 6, 'time': 2, 'output': '00101010', 'program': 'x[0] := 297' },
    { 'length': 6, 'time': 2, 'output': '00101011', 'program': 'x[0] := 298' },
    { 'length': 6, 'time': 2, 'output': '00101100', 'program': 'x[0] := 299' },
    { 'length': 6, 'time': 2, 'output': '00101101', 'program': 'x[0] := 300' },
    { 'length': 6, 'time': 2, 'output': '00101110', 'program': 'x[0] := 301' },
    { 'length': 6, 'time': 2, 'output': '00101111', 'program': 'x[0] := 302' },
    { 'length': 6, 'time': 2, 'output': '00110000', 'program': 'x[0] := 303' },
    { 'length': 6, 'time': 2, 'output': '00110001', 'program': 'x[0] := 304' },
    { 'length': 6, 'time': 2, 'output': '00110010', 'program': 'x[0] := 305' },
    { 'length': 6, 'time': 2, 'output': '00110011', 'program': 'x[0] := 306' },
    { 'length': 6, 'time': 2, 'output': '00110100', 'program': 'x[0] := 307' },
    { 'length': 6, 'time': 2, 'output': '00110101', 'program': 'x[0] := 308' },
    { 'length': 6, 'time': 2, 'output': '00110110', 'program': 'x[0] := 309' },
    { 'length': 6, 'time': 2, 'output': '00110111', 'program': 'x[0] := 310' },
    { 'length': 6, 'time': 2, 'output': '00111000', 'program': 'x[0] := 311' },
    { 'length': 6, 'time': 2, 'output': '00111001', 'program': 'x[0] := 312' },
    { 'length': 6, 'time': 2, 'output': '00111010', 'program': 'x[0] := 313' },
    { 'length': 6, 'time': 2, 'output': '00111011', 'program': 'x[0] := 314' },
    { 'length': 6, 'time': 2, 'output': '00111100', 'program': 'x[0] := 315' },
    { 'length': 6, 'time': 2, 'output': '00111101', 'program': 'x[0] := 316' },
    { 'length': 6, 'time': 2, 'output': '00111110', 'program': 'x[0] := 317' },
    { 'length': 6, 'time': 2, 'output': '00111111', 'program': 'x[0] := 318' },
    { 'length': 6, 'time': 2, 'output': '01000000', 'program': 'x[0] := 319' },
    { 'length': 6, 'time': 2, 'output': '01000001', 'program': 'x[0] := 320' },
    { 'length': 6, 'time': 2, 'output': '01000010', 'program': 'x[0] := 321' },
    { 'length': 6, 'time': 2, 'output': '01000011', 'program': 'x[0] := 322' },
    { 'length': 6, 'time': 2, 'output': '01000100', 'program': 'x[0] := 323' },
    { 'length': 6, 'time': 2, 'output': '01000101', 'program': 'x[0] := 324' },
    { 'length': 6, 'time': 2, 'output': '01000110', 'program': 'x[0] := 325' },
    { 'length': 6, 'time': 2, 'output': '01000111', 'program': 'x[0] := 326' },
    { 'length': 6, 'time': 2, 'output': '01001000', 'program': 'x[0] := 327' },
    { 'length': 6, 'time': 2, 'output': '01001001', 'program': 'x[0] := 328' },
    { 'length': 6, 'time': 2, 'output': '01001010', 'program': 'x[0] := 329' },
    { 'length': 6, 'time': 2, 'output': '01001011', 'program': 'x[0] := 330' },
    { 'length': 6, 'time': 2, 'output': '01001100', 'program': 'x[0] := 331' },
    { 'length': 6, 'time': 2, 'output': '01001101', 'program': 'x[0] := 332' },
    { 'length': 6, 'time': 2, 'output': '01001110', 'program': 'x[0] := 333' },
    { 'length': 6, 'time': 2, 'output': '01001111', 'program': 'x[0] := 334' },
    { 'length': 6, 'time': 2, 'output': '01010000', 'program': 'x[0] := 335' },
    { 'length': 6, 'time': 2, 'output': '01010001', 'program': 'x[0] := 336' },
    { 'length': 6, 'time': 2, 'output': '01010010', 'program': 'x[0] := 337' },
    { 'length': 6, 'time': 2, 'output': '01010011', 'program': 'x[0] := 338' },
    { 'length': 6, 'time': 2, 'output': '01010100', 'program': 'x[0] := 339' },
    { 'length': 6, 'time': 2, 'output': '01010101', 'program': 'x[0] := 340' },
    { 'length': 6, 'time': 2, 'output': '01010110', 'program': 'x[0] := 341' },
    { 'length': 6, 'time': 2, 'output': '01010111', 'program': 'x[0] := 342' },
    { 'length': 6, 'time': 2, 'output': '01011000', 'program': 'x[0] := 343' },
    { 'length': 6, 'time': 2, 'output': '01011001', 'program': 'x[0] := 344' },
    { 'length': 6, 'time': 2, 'output': '01011010', 'program': 'x[0] := 345' },
    { 'length': 6, 'time': 2, 'output': '01011011', 'program': 'x[0] := 346' },
    { 'length': 6, 'time': 2, 'output': '01011100', 'program': 'x[0] := 347' },
    { 'length': 6, 'time': 2, 'output': '01011101', 'program': 'x[0] := 348' },
    { 'length': 6, 'time': 2, 'output': '01011110', 'program': 'x[0] := 349' },
    { 'length': 6, 'time': 2, 'output': '01011111', 'program': 'x[0] := 350' },
    { 'length': 6, 'time': 2, 'output': '01100000', 'program': 'x[0] := 351' },
    { 'length': 6, 'time': 2, 'output': '01100001', 'program': 'x[0] := 352' },
    { 'length': 6, 'time': 2, 'output': '01100010', 'program': 'x[0] := 353' },
    { 'length': 6, 'time': 2, 'output': '01100011', 'program': 'x[0] := 354' },
    { 'length': 6, 'time': 2, 'output': '01100100', 'program': 'x[0] := 355' },
    { 'length': 6, 'time': 2, 'output': '01100101', 'program': 'x[0] := 356' },
    { 'length': 6, 'time': 2, 'output': '01100110', 'program': 'x[0] := 357' },
    { 'length': 6, 'time': 2, 'output': '01100111', 'program': 'x[0] := 358' },
    { 'length': 6, 'time': 2, 'output': '01101000', 'program': 'x[0] := 359' },
    { 'length': 6, 'time': 2, 'output': '01101001', 'program': 'x[0] := 360' },
    { 'length': 6, 'time': 2, 'output': '01101010', 'program': 'x[0] := 361' },
    { 'length': 6, 'time': 2, 'output': '01101011', 'program': 'x[0] := 362' },
    { 'length': 6, 'time': 2, 'output': '01101100', 'program': 'x[0] := 363' },
    { 'length': 6, 'time': 2, 'output': '01101101', 'program': 'x[0] := 364' },
    { 'length': 6, 'time': 2, 'output': '01101110', 'program': 'x[0] := 365' },
    { 'length': 6, 'time': 2, 'output': '01101111', 'program': 'x[0] := 366' },
    { 'length': 6, 'time': 2, 'output': '01110000', 'program': 'x[0] := 367' },
    { 'length': 6, 'time': 2, 'output': '01110001', 'program': 'x[0] := 368' },
    { 'length': 6, 'time': 2, 'output': '01110010', 'program': 'x[0] := 369' },
    { 'length': 6, 'time': 2, 'output': '01110011', 'program': 'x[0] := 370' },
    { 'length': 6, 'time': 2, 'output': '01110100', 'program': 'x[0] := 371' },
    { 'length': 6, 'time': 2, 'output': '01110101', 'program': 'x[0] := 372' },
    { 'length': 6, 'time': 2, 'output': '01110110', 'program': 'x[0] := 373' },
    { 'length': 6, 'time': 2, 'output': '01110111', 'program': 'x[0] := 374' },
    { 'length': 6, 'time': 2, 'output': '01111000', 'program': 'x[0] := 375' },
    { 'length': 6, 'time': 2, 'output': '01111001', 'program': 'x[0] := 376' },
    { 'length': 6, 'time': 2, 'output': '01111010', 'program': 'x[0] := 377' },
    { 'length': 6, 'time': 2, 'output': '01111011', 'program': 'x[0] := 378' },
    { 'length': 6, 'time': 2, 'output': '01111100', 'program': 'x[0] := 379' },
    { 'length': 6, 'time': 2, 'output': '01111101', 'program': 'x[0] := 380' },
    { 'length': 6, 'time': 2, 'output': '01111110', 'program': 'x[0] := 381' },
    { 'length': 6, 'time': 2, 'output': '01111111', 'program': 'x[0] := 382' },
    { 'length': 6, 'time': 2, 'output': '10000000', 'program': 'x[0] := 383' },
    { 'length': 6, 'time': 2, 'output': '10000001', 'program': 'x[0] := 384' },
    { 'length': 6, 'time': 2, 'output': '10000010', 'program': 'x[0] := 385' },
    { 'length': 6, 'time': 2, 'output': '10000011', 'program': 'x[0] := 386' },
    { 'length': 6, 'time': 2, 'output': '10000100', 'program': 'x[0] := 387' },
    { 'length': 6, 'time': 2, 'output': '10000101', 'program': 'x[0] := 388' },
    { 'length': 6, 'time': 2, 'output': '10000110', 'program': 'x[0] := 389' },
    { 'length': 6, 'time': 2, 'output': '10000111', 'program': 'x[0] := 390' },
    { 'length': 6, 'time': 2, 'output': '10001000', 'program': 'x[0] := 391' },
    { 'length': 6, 'time': 2, 'output': '10001001', 'program': 'x[0] := 392' },
    { 'length': 6, 'time': 2, 'output': '10001010', 'program': 'x[0] := 393' },
    { 'length': 6, 'time': 2, 'output': '10001011', 'program': 'x[0] := 394' },
    { 'length': 6, 'time': 2, 'output': '10001100', 'program': 'x[0] := 395' },
    { 'length': 6, 'time': 2, 'output': '10001101', 'program': 'x[0] := 396' },
    { 'length': 6, 'time': 2, 'output': '10001110', 'program': 'x[0] := 397' },
    { 'length': 6, 'time': 2, 'output': '10001111', 'program': 'x[0] := 398' },
    { 'length': 6, 'time': 2, 'output': '10010000', 'program': 'x[0] := 399' },
    { 'length': 6, 'time': 2, 'output': '10010001', 'program': 'x[0] := 400' },
    { 'length': 6, 'time': 2, 'output': '10010010', 'program': 'x[0] := 401' },
    { 'length': 6, 'time': 2, 'output': '10010011', 'program': 'x[0] := 402' },
    { 'length': 6, 'time': 2, 'output': '10010100', 'program': 'x[0] := 403' },
    { 'length': 6, 'time': 2, 'output': '10010101', 'program': 'x[0] := 404' },
    { 'length': 6, 'time': 2, 'output': '10010110', 'program': 'x[0] := 405' },
    { 'length': 6, 'time': 2, 'output': '10010111', 'program': 'x[0] := 406' },
    { 'length': 6, 'time': 2, 'output': '10011000', 'program': 'x[0] := 407' },
    { 'length': 6, 'time': 2, 'output': '10011001', 'program': 'x[0] := 408' },
    { 'length': 6, 'time': 2, 'output': '10011010', 'program': 'x[0] := 409' },
    { 'length': 6, 'time': 2, 'output': '10011011', 'program': 'x[0] := 410' },
    { 'length': 6, 'time': 2, 'output': '10011100', 'program': 'x[0] := 411' },
    { 'length': 6, 'time': 2, 'output': '10011101', 'program': 'x[0] := 412' },
    { 'length': 6, 'time': 2, 'output': '10011110', 'program': 'x[0] := 413' },
    { 'length': 6, 'time': 2, 'output': '10011111', 'program': 'x[0] := 414' },
    { 'length': 6, 'time': 2, 'output': '10100000', 'program': 'x[0] := 415' },
    { 'length': 6, 'time': 2, 'output': '10100001', 'program': 'x[0] := 416' },
    { 'length': 6, 'time': 2, 'output': '10100010', 'program': 'x[0] := 417' },
    { 'length': 6, 'time': 2, 'output': '10100011', 'program': 'x[0] := 418' },
    { 'length': 6, 'time': 2, 'output': '10100100', 'program': 'x[0] := 419' },
    { 'length': 6, 'time': 2, 'output': '10100101', 'program': 'x[0] := 420' },
    { 'length': 6, 'time': 2, 'output': '10100110', 'program': 'x[0] := 421' },
    { 'length': 6, 'time': 2, 'output': '10100111', 'program': 'x[0] := 422' },
    { 'length': 6, 'time': 2, 'output': '10101000', 'program': 'x[0] := 423' },
    { 'length': 6, 'time': 2, 'output': '10101001', 'program': 'x[0] := 424' },
    { 'length': 6, 'time': 2, 'output': '10101010', 'program': 'x[0] := 425' },
    { 'length': 6, 'time': 2, 'output': '10101011', 'program': 'x[0] := 426' },
    { 'length': 6, 'time': 2, 'output': '10101100', 'program': 'x[0] := 427' },
    { 'length': 6, 'time': 2, 'output': '10101101', 'program': 'x[0] := 428' },
    { 'length': 6, 'time': 2, 'output': '10101110', 'program': 'x[0] := 429' },
    { 'length': 6, 'time': 2, 'output': '10101111', 'program': 'x[0] := 430' },
    { 'length': 6, 'time': 2, 'output': '10110000', 'program': 'x[0] := 431' },
    { 'length': 6, 'time': 2, 'output': '10110001', 'program': 'x[0] := 432' },
    { 'length': 6, 'time': 2, 'output': '10110010', 'program': 'x[0] := 433' },
    { 'length': 6, 'time': 2, 'output': '10110011', 'program': 'x[0] := 434' },
    { 'length': 6, 'time': 2, 'output': '10110100', 'program': 'x[0] := 435' },
    { 'length': 6, 'time': 2, 'output': '10110101', 'program': 'x[0] := 436' },
    { 'length': 6, 'time': 2, 'output': '10110110', 'program': 'x[0] := 437' },
    { 'length': 6, 'time': 2, 'output': '10110111', 'program': 'x[0] := 438' },
    { 'length': 6, 'time': 2, 'output': '10111000', 'program': 'x[0] := 439' },
    { 'length': 6, 'time': 2, 'output': '10111001', 'program': 'x[0] := 440' },
    { 'length': 6, 'time': 2, 'output': '10111010', 'program': 'x[0] := 441' },
    { 'length': 6, 'time': 2, 'output': '10111011', 'program': 'x[0] := 442' },
    { 'length': 6, 'time': 2, 'output': '10111100', 'program': 'x[0] := 443' },
    { 'length': 6, 'time': 2, 'output': '10111101', 'program': 'x[0] := 444' },
    { 'length': 6, 'time': 2, 'output': '10111110', 'program': 'x[0] := 445' },
    { 'length': 6, 'time': 2, 'output': '10111111', 'program': 'x[0] := 446' },
    { 'length': 6, 'time': 2, 'output': '11000000', 'program': 'x[0] := 447' },
    { 'length': 6, 'time': 2, 'output': '11000001', 'program': 'x[0] := 448' },
    { 'length': 6, 'time': 2, 'output': '11000010', 'program': 'x[0] := 449' },
    { 'length': 6, 'time': 2, 'output': '11000011', 'program': 'x[0] := 450' },
    { 'length': 6, 'time': 2, 'output': '11000100', 'program': 'x[0] := 451' },
    { 'length': 6, 'time': 2, 'output': '11000101', 'program': 'x[0] := 452' },
    { 'length': 6, 'time': 2, 'output': '11000110', 'program': 'x[0] := 453' },
    { 'length': 6, 'time': 2, 'output': '11000111', 'program': 'x[0] := 454' },
    { 'length': 6, 'time': 2, 'output': '11001000', 'program': 'x[0] := 455' },
    { 'length': 6, 'time': 2, 'output': '11001001', 'program': 'x[0] := 456' },
    { 'length': 6, 'time': 2, 'output': '11001010', 'program': 'x[0] := 457' },
    { 'length': 6, 'time': 2, 'output': '11001011', 'program': 'x[0] := 458' },
    { 'length': 6, 'time': 2, 'output': '11001100', 'program': 'x[0] := 459' },
    { 'length': 6, 'time': 2, 'output': '11001101', 'program': 'x[0] := 460' },
    { 'length': 6, 'time': 2, 'output': '11001110', 'program': 'x[0] := 461' },
    { 'length': 6, 'time': 2, 'output': '11001111', 'program': 'x[0] := 462' },
    { 'length': 6, 'time': 2, 'output': '11010000', 'program': 'x[0] := 463' },
    { 'length': 6, 'time': 2, 'output': '11010001', 'program': 'x[0] := 464' },
    { 'length': 6, 'time': 2, 'output': '11010010', 'program': 'x[0] := 465' },
    { 'length': 6, 'time': 2, 'output': '11010011', 'program': 'x[0] := 466' },
    { 'length': 6, 'time': 2, 'output': '11010100', 'program': 'x[0] := 467' },
    { 'length': 6, 'time': 2, 'output': '11010101', 'program': 'x[0] := 468' },
    { 'length': 6, 'time': 2, 'output': '11010110', 'program': 'x[0] := 469' },
    { 'length': 6, 'time': 2, 'output': '11010111', 'program': 'x[0] := 470' },
    { 'length': 6, 'time': 2, 'output': '11011000', 'program': 'x[0] := 471' },
    { 'length': 6, 'time': 2, 'output': '11011001', 'program': 'x[0] := 472' },
    { 'length': 6, 'time': 2, 'output': '11011010', 'program': 'x[0] := 473' },
    { 'length': 6, 'time': 2, 'output': '11011011', 'program': 'x[0] := 474' },
    { 'length': 6, 'time': 2, 'output': '11011100', 'program': 'x[0] := 475' },
    { 'length': 6, 'time': 2, 'output': '11011101', 'program': 'x[0] := 476' },
    { 'length': 6, 'time': 2, 'output': '11011110', 'program': 'x[0] := 477' },
    { 'length': 6, 'time': 2, 'output': '11011111', 'program': 'x[0] := 478' },
    { 'length': 6, 'time': 2, 'output': '11100000', 'program': 'x[0] := 479' },
    { 'length': 6, 'time': 2, 'output': '11100001', 'program': 'x[0] := 480' },
    { 'length': 6, 'time': 2, 'output': '11100010', 'program': 'x[0] := 481' },
    { 'length': 6, 'time': 2, 'output': '11100011', 'program': 'x[0] := 482' },
    { 'length': 6, 'time': 2, 'output': '11100100', 'program': 'x[0] := 483' },
    { 'length': 6, 'time': 2, 'output': '11100101', 'program': 'x[0] := 484' },
    { 'length': 6, 'time': 2, 'output': '11100110', 'program': 'x[0] := 485' },
    { 'length': 6, 'time': 2, 'output': '11100111', 'program': 'x[0] := 486' },
    { 'length': 6, 'time': 2, 'output': '11101000', 'program': 'x[0] := 487' },
    { 'length': 6, 'time': 2, 'output': '11101001', 'program': 'x[0] := 488' },
    { 'length': 6, 'time': 2, 'output': '11101010', 'program': 'x[0] := 489' },
    { 'length': 6, 'time': 2, 'output': '11101011', 'program': 'x[0] := 490' },
    { 'length': 6, 'time': 2, 'output': '11101100', 'program': 'x[0] := 491' },
    { 'length': 6, 'time': 2, 'output': '11101101', 'program': 'x[0] := 492' },
    { 'length': 6, 'time': 2, 'output': '11101110', 'program': 'x[0] := 493' },
    { 'length': 6, 'time': 2, 'output': '11101111', 'program': 'x[0] := 494' },
    { 'length': 6, 'time': 2, 'output': '11110000', 'program': 'x[0] := 495' },
    { 'length': 6, 'time': 2, 'output': '11110001', 'program': 'x[0] := 496' },
    { 'length': 6, 'time': 2, 'output': '11110010', 'program': 'x[0] := 497' },
    { 'length': 6, 'time': 2, 'output': '11110011', 'program': 'x[0] := 498' },
    { 'length': 6, 'time': 2, 'output': '11110100', 'program': 'x[0] := 499' },
    { 'length': 6, 'time': 2, 'output': '11110101', 'program': 'x[0] := 500' },
    { 'length': 6, 'time': 2, 'output': '11110110', 'program': 'x[0] := 501' },
    { 'length': 6, 'time': 2, 'output': '11110111', 'program': 'x[0] := 502' },
    { 'length': 6, 'time': 2, 'output': '11111000', 'program': 'x[0] := 503' },
    { 'length': 6, 'time': 2, 'output': '11111001', 'program': 'x[0] := 504' },
    { 'length': 6, 'time': 2, 'output': '11111010', 'program': 'x[0] := 505' },
    { 'length': 6, 'time': 2, 'output': '11111011', 'program': 'x[0] := 506' },
    { 'length': 6, 'time': 2, 'output': '11111100', 'program': 'x[0] := 507' },
    { 'length': 6, 'time': 2, 'output': '11111101', 'program': 'x[0] := 508' },
    { 'length': 6, 'time': 2, 'output': '11111110', 'program': 'x[0] := 509' },
    { 'length': 6, 'time': 2, 'output': '11111111', 'program': 'x[0] := 510' },
]
