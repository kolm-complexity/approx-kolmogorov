import React from 'react'
import { BlockMath, InlineMath } from 'react-katex'
import {
    Container,
    Header,
    List,
    Segment,
    Table,
} from 'semantic-ui-react'

const ResourcesIMP = () => {
    return (
        <main>
            <Segment
                inverted
                textAlign='center'
                padded
            >
                <Container text>
                    <Header as='h2' inverted>
                        The IMP Programming Language
                    </Header>
                </Container>
            </Segment>
            <Segment vertical>
                <Container text>
                    <p>
                        For our programming language we decided on IMP,
                        a simple textbook example of an imperative programming
                        language [17] that is Turing-complete.
                    </p>
                    <p>
                        The syntax of IMP is given by the following grammar.
                    </p>
                    <GrammarIMP />
                    <p>
                        We are interested in single binary
                        strings as the output of programs,
                        adopting the following convention:
                    </p>
                    <ol>
                        <li>We map the register content to a binary string in canonical order</li>
                        <li>Concatenation of all registers' strings as pogram output</li>
                    </ol>
                    <p>
                        For example, considering the following program:
                    </p>
                    <pre>{sampleimp}</pre>
                    <p>
                        Then, register 0 is converted to <code>1</code>, register 1 is converted to <code>0</code> and register 2 is converted to <code>00</code>.
                    </p>
                    <List>
                        <List.Item>
                            <code>x[0] := 2</code>
                            <InlineMath math='\quad\mapsto\quad' />
                            <code>1</code>
                        </List.Item>
                        <List.Item>
                            <code>x[1] := 1</code>
                            <InlineMath math='\quad\mapsto\quad' />
                            <code>0</code>
                        </List.Item>
                        <List.Item>
                            <code>x[2] := 3</code>
                            <InlineMath math='\quad\mapsto\quad' />
                            <code>00</code>
                        </List.Item>
                    </List>
                    <p>
                        Then the output string is calculated as
                    </p>
                    <BlockMath math='\mathtt{1} + \mathtt{0} + \mathtt{00} + \epsilon + \epsilon + \dots = \mathtt{1000}' />
                </Container>
            </Segment>
            <Segment vertical>
                <Container text>
                    <p>
                        We call the length of an IMP program to the number of terminal
                        symbols, such as <code>skip</code>, <code>while</code>,
                        digits of numbers, etc.
                    </p>
                    <p>
                        The following table shows both the number of programs of
                        a given length and the accumulated count upto a length.
                    </p>
                    <Table
                        celled
                        selectable
                        compact='very'
                        collapsing
                        textAlign='center'
                        className='force-center'
                    >
                        <Table.Header className='sans'>
                            <Table.Row>
                                {lentab.headers.map((x, i) => (
                                    <Table.HeaderCell key={x + i}>{x}</Table.HeaderCell>
                                ))}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {lentab.rows.map((xs, i) => (
                                <Table.Row key={'_' + i}>
                                    {xs.map((x, j) => (
                                        <Table.Cell key={'_' + i + '_' + j}>
                                            {x.toLocaleString()}
                                        </Table.Cell>
                                    ))}
                                </Table.Row>
                            ))}
                        </Table.Body>
                    </Table>
                </Container>
            </Segment>
        </main>
    )
}

export default ResourcesIMP


const GrammarIMP = () => (
    <div className='grammar'>
        <div className='productions'>
            <span className='var'>P</span>
            <div className='rules'>
                <span>skip</span>
                <span><span className='var'>X</span> := <span className='var'>A</span></span>
                <span>(<span className='var'>P</span> ; <span className='var'>P</span>)</span>
                <span>(if <span className='var'>B</span> then <span className='var'>P</span> else <span className='var'>P</span>)</span>
                <span>(while <span className='var'>B</span> do <span className='var'>P</span>)</span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>A</span>
            <div className='rules'>
                <span className='var'>N</span>
                <span className='var'>X</span>
                <span>(<span className='var'>A</span> + <span className='var'>A</span>)</span>
                <span>(<span className='var'>A</span> - <span className='var'>A</span>)</span>
                <span>(<span className='var'>A</span> * <span className='var'>A</span>)</span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>B</span>
            <div className='rules'>
                <span>true</span>
                <span>false</span>
                <span>(<span className='var'>A</span> = <span className='var'>A</span>)</span>
                <span>(<span className='var'>A</span> {'<'} <span className='var'>A</span>)</span>
                <span>¬ <span className='var'>B</span></span>
                <span>(<span className='var'>B</span> ∨ <span className='var'>B</span>)</span>
                <span>(<span className='var'>B</span> ∧ <span className='var'>B</span>)</span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>X</span>
            <div className='rules'>
                <span>x[<span className='var'>N</span>]</span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>N</span>
            <div className='rules'>
                <span>0</span>
                <span className='var'>C</span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>C</span>
            <div className='rules'>
                <span>1<span className='var'>S</span></span>
                <span>2<span className='var'>S</span></span>
                <span>3<span className='var'>S</span></span>
                <span>4<span className='var'>S</span></span>
                <span>5<span className='var'>S</span></span>
                <span>6<span className='var'>S</span></span>
                <span>7<span className='var'>S</span></span>
                <span>8<span className='var'>S</span></span>
                <span>9<span className='var'>S</span></span>
            </div>
        </div>
        <div className='productions'>
            <span className='var'>S</span>
            <div className='rules'>
                <span>0<span className='var'>S</span></span>
                <span>1<span className='var'>S</span></span>
                <span>2<span className='var'>S</span></span>
                <span>3<span className='var'>S</span></span>
                <span>4<span className='var'>S</span></span>
                <span>5<span className='var'>S</span></span>
                <span>6<span className='var'>S</span></span>
                <span>7<span className='var'>S</span></span>
                <span>8<span className='var'>S</span></span>
                <span>9<span className='var'>S</span></span>
                <span><InlineMath math='\epsilon' /></span>
            </div>
        </div>
    </div>
)

const sampleimp = `
(x[0] := 2;
(x[1] := 1;
 x[2] := 3))
`.trim()

const lentab = {
    headers: ['Length', 'Count', 'Accumulated'],
    rows: [
        [0, 0, 0],
        [1, 1, 1],
        [2, 0, 1],
        [3, 3, 4],
        [4, 104, 108],
        [5, 2124, 2232],
        [6, 35770, 38002],
        [7, 546611, 584613],
        [8, 7991176, 8575789],
        [9, 114513832, 123089621],
        [10, 1631934090, 1755023711],
        [11, 23318957744, 25073981455],
        [12, 335696750370, 360770731825],
        [13, 4880779005146, 5241549736971],
        [14, 71741423459678, 76982973196649],
        [15, 1066136368277130, 1143119341473780],
        [16, 16010086272979500, 17153205614453200],
        [17, 242748521456566000, 259901727071019000],
        [18, 3712709600934420000, 3972611328005440000],
        [19, 57224417337984700000, 61197028665990100000],
        [20, 888047210962447000000, 949244239628437000000],
        [21, 13864404258572500000000, 14813648498201000000000],
        [22, 217603512290629000000000, 232417160788830000000000],
        [23, 3431321486215440000000000, 3663738647004270000000000],
        [24, 54332262704370800000000000, 57996001351375100000000000],
        [25, 863493685297937000000000000, 921489686649312000000000000],
        [26, 13768885718181600000000000000, 14690375404830900000000000000],
        [27, 220208697497955000000000000000, 234899072902786000000000000000],
        [28, 3531379677198370000000000000000, 3766278750101160000000000000000],
    ],
}
