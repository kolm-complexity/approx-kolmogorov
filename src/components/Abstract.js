import React from 'react'

import {
    Container,
    Header,
} from 'semantic-ui-react'

const abstract = `
Motivated by algorithmic information theory, the problem of program
discovery can help understand underlying generative mechanisms of
natural and artificial phenomena. The uncomputability of such inverse
problem, however, significantly restricts a wider application of
exhaustive methods. Here we present a proof of concept of an approach
based on a high-level imperative programming language. Its main
advantage is that conceptually complex computational routines are more
succinctly expressed, unlike lower-level models such as Turing
machines or cellular automata. Here, we present a new approach that
contributes to model discovery and novel systematic approaches to
estimations of algorithmic complexity.
`

const keywords = [
    'Kolmogorov-Chaitin complexity',
    'Algorithmic Information Theory',
    'algorithmic randomness',
    'generative programming',
    'model generation',
]

const Abstract = () => (
    <Container
        text
        textAlign='justified'
        style={{paddingTop: '2rem'}}
    >
        <Header as='h2' textAlign='center'>
            Abstract
        </Header>
        <p>{abstract}</p>
        <p><em>Keywords:</em> {keywords.join(', ')}.</p>
    </Container>
)

export default Abstract
