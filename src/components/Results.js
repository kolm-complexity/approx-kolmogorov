import React, { Fragment } from 'react'
import { InlineMath } from 'react-katex'

import {
    Divider,
    Segment,
    Grid,
} from 'semantic-ui-react'

import PageCard from './PageCard'

const Results = () => (
    <Fragment>
        <Divider
            as='h2'
            className='header'
            horizontal
            style={{paddingTop: '2rem'}}
        >
            Results
        </Divider>
        <Segment vertical basic>
            <Grid celled='internally' columns='equal' stackable>
                <Grid.Row textAlign='center'>
                    <Grid.Column id='s9'>
                        <PageCard
                            header={
                                <Fragment>
                                    Sampling and executing programs in{' '}
                                    <InlineMath math={`\\mathcal{S}_9`} />
                                </Fragment>
                            }
                            description={
                                <Fragment>
                                    Results for estimating a suitable
                                    running time threshold{' '}
                                    <InlineMath math={'\\mathbf{T}'} />
                                    {' '}and all programs up to length 9.
                                </Fragment>
                            }
                            label='Read more'
                            to='s9'
                        />
                    </Grid.Column>
                    <Grid.Column id='lookahead'>
                        <PageCard
                            header='Looking ahead the program space'
                            description={`Improving the trivial upper bounds for
                            Kolmogorov complexity by looking
                            ahead the program space.`}
                            label='Read more'
                            to='lookahead'
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    </Fragment >
)

export default Results
