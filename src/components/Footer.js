import React from 'react'

import {
    Container,
    Grid,
} from 'semantic-ui-react'

const Footer = () => (
    <Container
        style={{
            paddingTop: '2rem',
            paddingBottom: '2rem'
        }}
    >
        <Grid divided inverted stackable>
            <Grid.Row>
                <Grid.Column width={2}>
                </Grid.Column>
                <Grid.Column width={7}>
                    {/*
                    <Header as='h4' inverted>
                        Contact information
                    </Header>
                    <p>
                        We could at least put a name and email address here...bla bla bla.
                    </p>
                     */}
                </Grid.Column>
                <Grid.Column width={7}>
                    {/*
                    <Header as='h4' inverted>
                        About
                    </Header>
                    <p>
                        Article submited to <em>placeholder</em> journal...bla bla bla.
                    </p>
                     */}
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </Container>
)

export default Footer
