import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'

import {
    Header,
    Button,
} from 'semantic-ui-react'

const PageCard = ({ header, description, label, disabled, to }) => (
    <Fragment>
        <Header as='h3'>
            { header }
        </Header>
        <p className='pagecard_desc'>
            {description}
        </p>
        <Button
            className='pagecard_btn'
            secondary
            inverted
            size='small'
            disabled={disabled}
            as={Link}
            to={to || '#'}
        >
            { label }
        </Button>
    </Fragment>
)

export default PageCard
