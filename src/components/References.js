import React, { useState } from 'react'

import {
    Container,
    Header,
    Accordion,
    Icon,
    List,
} from 'semantic-ui-react'


const references = [
    {
        key: 'Calude-Stay:2008',
        authors: 'C. Calude and M. Stay',
        title: 'Most programs stop quickly or never halt',
        year: 2008,
        url: 'https://doi.org/10.1016/j.aam.2007.01.001',
    },
    {
        key: 'Calude-Dumitrescu:2018',
        authors: 'C. Calude and M. Dumitrescu',
        title: 'A probabilistic anytime algorithm for the halting problem.',
        year: 2018,
        url: 'https://doi.org/10.3233/COM-170073',
    },
    {
        key: 'Calude-Dumitrescu:2020',
        authors: 'C. Calude and M. Dumitrescu',
        title: 'A statistical anytime algorithm for the Halting Problem',
        year: 2020,
        url: 'https://doi.org/10.3233/COM-190250',
    },
    {
        key: 'cantor_beitrag_1877',
        authors: 'G. Cantor',
        title: 'Ein Beitrag zur Mannigfaltigkeitslehre',
        year: 1877,
        url: 'https://doi.org/10.1515/crelle-1878-18788413',
    },
    {
        key: 'Chaitin:1975',
        authors: 'G.J. Chaitin',
        title: 'A theory of program size formally identical to information theory',
        year: 1975,
        url: 'https://doi.org/10.1145/321892.321894',
    },
    {
        key: 'Cibej:2014',
        authors: 'U. Čibej, B. Robič and J. Mihelič',
        title: 'Empirical estimation of the halting probabilities',
        year: 2014,
        url: 'https://www.researchgate.net/profile/Jurij-Mihelic/publication/267327970_Empirical_Estimation_of_the_Halting_Probabilities/links/5754733008ae10d9337a3d99/Empirical-Estimation-of-the-Halting-Probabilities.pdf',
    },
    {
        key: 'cilibrasi:vitany',
        authors: 'R.L. Cilibrasi and P. Vitányi',
        title: 'Clustering by compression',
        year: 2005,
        url: 'https://doi.org/10.1109/TIT.2005.844059',
    },
    {
        key: 'sloan',
        authors: 'N. Gauvrit, J.-P. Delahaye and H. Zenil',
        title: 'Sloane’s Gap: Do Mathematical and Social Factors Explain the Distribution of Numbers in the OEIS?',
        year: 2013,
        url: 'https://doi.org/10.5642/jhummath.201301.03',
    },
    {
        key: 'Grass:1996',
        authors: 'J. Grass',
        title: 'Reasoning about computational resource allocation. An introduction to anytime algorithms',
        year: 1996,
        url: 'https://doi.org/10.1145/332148.332154',
    },
    {
        key: 'hoeffding',
        authors: 'W. Hoeffding',
        title: 'Probability inequalities for sums of bounded random variables',
        year: 1963,
        url: 'https://doi.org/10.1080/01621459.1963.10500830',
    },
    {
        key: 'Kohler:2005',
        authors: 'S. Köhler, C. Schindelhauer and M. Ziegler',
        title: 'On Approximating Real-World Halting Problems',
        year: 2005,
        url: 'https://doi.org/10.1007/11537311_40',
    },
    {
        key: 'kolmogorov',
        authors: 'A.N. Kolmogorov',
        title: 'Three approaches to the quantitative definition of information',
        year: 1965,
        url: 'https://doi.org/10.1080/00207166808803030',
    },
    {
        key: 'li:vitany',
        authors: 'M. Li and P. Vitányi',
        title: 'An Introduction to Kolmogorov Complexity and Its Applications',
        year: 2008,
        url: 'https://link.springer.com/book/10.1007/978-0-387-49820-1',
    },
    {
        key: 'plotkin',
        authors: 'G.D. Plotkin',
        title: 'Structural operational semantics',
        year: 1981,
        url: 'http://homepages.inf.ed.ac.uk/gdp/publications/sos_jlap.pdf',
    },
    {
        key: 'Soler-Toscano:2014',
        authors: 'F. Soler-Toscano F, H. Zenil, J.-P. Delahaye and N. Gauvrit',
        title: 'Calculating Kolmogorov Complexity from the Output Frequency Distributions of Small Turing Machines',
        year: 2014,
        url: 'https://doi.org/10.1371/journal.pone.0096223',
    },
    {
        key: 'turing',
        authors: 'A.M. Turing',
        title: 'On Computable Numbers, with an Application to the Entscheidungsproblem',
        year: 1937,
        url: 'https://doi.org/10.1112/plms/s2-42.1.230',
    },
    {
        key: 'winskel',
        authors: 'G. Winskel',
        title: 'The Formal Semantics of Programming Languages',
        year: 1993,
        url: 'https://mitpress.mit.edu/books/formal-semantics-programming-languages',
    },
    {
        key: 'zenil:bdm',
        authors: 'H. Zenil, S. Hernández-Orozco, N.A. Kiani, F. Soler-Toscano and A. Rueda-Toice',
        title: 'A Decomposition Method for Global Evaluation of Shannon Entropy and Local Estimations of Algorithmic Complexity',
        year: 2018,
        url: 'https://doi.org/10.3390/e20080605',
    },
    {
        key: 'zenil:sub-turing',
        authors: 'H. Zenil, L. Badillo, S. Hernández-Orozco, F. Hernández-Quiroz',
        title: 'Coding-theorem Like Behaviour and Emergence of the Universal Distribution from Resource-bounded Algorithmic Probability',
        year: 2018,
        url: 'https://doi.org/10.1080/17445760.2018.1448932',
    },
    {
        key: 'zenil:nature',
        authors: 'H. Zenil, N.A. Kiani, A. Zea, J. Tegnér',
        title: 'Causal Deconvolution by Algorithmic Generative Models',
        year: 2019,
        url: 'https://doi.org/10.1038/s42256-018-0005-0',
    },
    {
        key: 'zenil:iscience',
        authors: 'H. Zenil, N.A. Kiani, F. Marabita, Y. Deng, S. Elias, A. Schmidt, G. Ball, J. Tegnér',
        title: 'An Algorithmic Information Calculus for Causal Discovery and Reprogramming Systems',
        year: 2019,
        url: 'https://doi.org/10.1016/j.isci.2019.07.043',
    },
    {
        key: 'review:complexity:zenil',
        authors: 'H. Zenil',
        title: 'A Review of Methods for Estimating Algorithmic Complexity: Options, Challenges, and New Directions',
        year: 2020,
        url: 'https://doi.org/10.3390/e22060612',
    },
    {
        key: 'frontiers',
        authors: 'S. Hernández-Orozco, H. Zenil, J. Riedel, A. Uccello , N.A. Kiani, and J. Tegnér',
        title: 'Algorithmic Probability-guided Machine Learning On Non-differentiable Spaces',
        year: 2021,
        url: 'https://doi.org/10.3389/frai.2020.567356',
    },
    {
        key: 'algodyn',
        authors: 'H. Zenil, N.A. Kiani, F.S. Abrahao and J. Tegnér',
        title: 'Algorithmic Information Dynamics',
        year: 2020,
        url: 'https://doi.org/10.4249/scholarpedia.53143',
    },
    {
        key: 'noauthor_automacoin_nodate',
        authors: null,
        title: 'AUTOMACOIN',
        year: null,
        url: 'https://www.automacoin.com/',
    },
    {
        key: 'noauthor_haskell_nodate',
        authors: null,
        title: 'Haskell Language',
        year: null,
        url: 'https://www.haskell.org/',
    },
]

const References = () => {
    const [active, setActive] = useState(false)
    return (
        <Container text style={{paddingTop: '2rem'}}>
            <Header as='h2' textAlign='center'>
                References
            </Header>

            {/* <p>Here we could highlight related work</p> */}
            
            <Accordion>
                <Accordion.Title
                    active={active}
                    onClick={() => setActive(!active)}
                >
                    <Icon name='dropdown' />
                    See all references
                </Accordion.Title>
                <Accordion.Content active={active}>
                    <List relaxed='very'>
                        {references.map(ref => (
                            <List.Item key={ref.key}>
                                <List.Content>
                                    <List.Header
                                        as={ref.url ? 'a' : null}
                                        href={ref.url || null}
                                        target={ref.url ? '_blank' : null}
                                        rel={ref.url ? 'noopener noreferer' : null}
                                    >
                                        {ref.title}
                                    </List.Header>
                                    <List.Description>
                                        <b>{ref.year}</b>{' '}
                                        {ref.authors}
                                    </List.Description>
                                </List.Content>
                            </List.Item>
                        ))}
                    </List>
                </Accordion.Content>
            </Accordion>
        </Container>
    )
}

export default References
