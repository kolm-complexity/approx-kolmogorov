import React from 'react'

import {
    Container,
    Header,
    List,
    Segment,
} from 'semantic-ui-react'

const paperTitle = `Computable Model Discovery and High-Level Programming Approximations
to Algorithmic Complexity`

const paperAuthors = [
    { name: 'Vladimir Lemus' },
    { name: 'Eduardo Acuña' },
    { name: 'Víctor Zamora' },
    { name: 'Francisco Hernandez-Quiroz' },
    { name: 'Hector Zenil' },
]

const PaperHeading = () => {
    const authorKey = ({ name }) => (
        'author_' + name.split(' ').map(x => x[0]).join('')
    )
    return (
        <Container text>
            <Segment basic vertical padded>
                <Header
                    as='h1'
                    content={paperTitle}
                    inverted
                />
            </Segment>
            <Segment basic vertical padded>
                <Header as='h4' inverted>
                    <List>
                        {paperAuthors.map(author => (
                            <List.Item key={authorKey(author)}>
                                {author.name}
                            </List.Item>
                        ))}
                    </List>
                </Header>
            </Segment>
            {/*
            <Segment basic vertical padded='very' inverted>
                Read the article:<br />
                <Button
                    inverted
                    size='medium'
                    disabled
                    as={Link}
                    to='/'
                >
                    Not yet available
                </Button>
                </Segment>
             */}
        </Container>
    )
}

export default PaperHeading
