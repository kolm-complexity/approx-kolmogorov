import React from 'react'

import { Image, ImageGroup } from 'semantic-ui-react'

const ChartGroup = ({ size, children }) => (
    <ImageGroup size={size} className='chart'>
        {children}
    </ImageGroup>
)

const Chart = ({ src, size }) => (
    <Image src={src} size={size} centered className='chart' />
)

export { Chart, ChartGroup }
