import React, { Fragment } from 'react'

import {
    Divider,
    Segment,
    Grid,
} from 'semantic-ui-react'

import PageCard from './PageCard'


const Resources = () => (
    <Fragment>
        <Divider
            as='h2'
            className='header'
            horizontal
            style={{paddingTop: '2rem'}}
        >
            Resources
        </Divider>
        <Segment vertical basic>
            <Grid celled='internally' columns={2} stackable>
                <Grid.Row textAlign='center'>
                    <Grid.Column id='imp'>
                        <PageCard
                            header='The IMP Programming Language'
                            description={`Syntax and semantics for the high-level
                            imperative language we're using as
                            the reference machine.`}
                            label='Read more'
                            to='/imp'
                        />
                    </Grid.Column>
                    {/*
                    <Grid.Column id='enums'>
                        <PageCard
                            header='Enumerations'
                            description={`Exploring the space of IMP programs by
                            ordering them by length
                            and lexicographically.`}
                            label='Not yet available'
                            disabled
                            to='/enums'
                        />
                     </Grid.Column>
                     */}
                </Grid.Row>
            </Grid>
        </Segment>
    </Fragment>
)

export default Resources
